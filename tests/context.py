#
# File used to give context to tests.
#

import os
import sys
sys.path.insert(
    0, os.path.abspath(os.path.join(os.path.dirname("__file__"), '..')))

import benders
