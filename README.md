# I, BranDec

An integer branch-and-benders-cut framework.

The point of this project is to make apparent (and usable, hopefully) the
separation between master problem and sub-problem during Benders decomposition.

All too often do I see, or myself code, a Benders algorithm where everything is
intertwined. The idea here is that the master problem does not need to know
about the sub-problem, or only in very vague terms. We need to set-up the
master problem's variables and constraints, but all the work done in the
sub-problem (from generating dual values to aggregating cuts) should not change
the master's behaviour.

The current implementation supports integer sub-problems. In which case, you
need to provide a heuristic as an integer solver during the B&B of the master
problem. Then you need to call `Benders::postprocessing()` to solve
sub-problems which fall in the integrality gap.

## Usage

For usage, the easiest is to follow the examples (see below). tl;dr:

 - Create a Benders instance: `Benders()`, and model your master problem
   `Benders::addVars(), Benders::addConstr()`.
 - Create your sub-problem istance(s): `subs[id] = Model(stuff)` where `Model`
   extends the base class `SubPb`.
 - Add the sub-problem(s) to Benders: `Benders::addSub()`.
 - Solve: `Benders::solve()`.
 - If your problem is integer, you need to provide:
   - a heuristic with `Benders::addUpper()`;
   - a MIP formulation with `Benders:addIntSub()`; and
   - call `Benders::postprocessing()` after.

### Tutorials

I will try to have more posts about using the framework, some info on my
[blog](http://arthur.maheo.net/):

 - [Benders primer.](http://arthur.maheo.net/a-short-introduction-to-benders/)
 - [Using BranDec.](http://arthur.maheo.net/modern-benders-in-python/)

### Logging

I use the `logging` module from
[Python](https://docs.python.org/3.5/library/logging.html) by creating a
"global" `Log` object that you can call upon, you can also set its level.

Logging works with an accumulation flag:

 - None: only outputs warnings and errors.
 - `-v`: output `INFO` level information (user output).
 - `-vv`: output everything.

### Debugging

There is a `debug` flag that can be set for extra checks during the execution.

### Examples

Three different examples:

 - `imrt_primal`: an example of using a primal sub-problem to do Intensity
   Modulated Radiation Therapy.
 - `flp_dual`: an example of using a dual sub-problem to do a Facility Location
   Problem.
 - `simple_int`: an example of a four variable integer program solved to
   optimality.

### Output

A sample of running `simple_int`:

```
Added 2 master variables.
Registered 1 sub-problems bundled in 1 cuts.
Registered 1 integer sub-problems.
No time limit defined.
No maximum number of iterations
 Itrs |   Master   |     q      |    q(z)    | Opt |     UB
 ---- + ---------- + ---------- + ---------- + --- + ----------
    1 |      0.000 |      0.000 |          - |     |          -
    2 |      6.000 |      0.000 |      1.500 |  X  |      2.000
 ---- + ---------- + ---------- + ---------- + --- + ----------
 Itrs |   Master   |     LP     | Int. Value | Opt |    Best
 ---- + ---------- + ---------- + ---------- + --- + ----------
    1 |      6.000 |      1.500 |      2.000 |  X  |      8.000
Best solution found (8.0), using: 10
```

The solution's report is then printed in JSON format.

```json
{"nb_feas_cuts": 1,
 "int_time": 0.00015900000000002024,
 "int_gap": 0.031249996093750492,
 "nb_nodes": 0,
 "solution": 8.0,
 "cutoff": false,
 "upper_time": 0.00019700000000000273,
 "nb_opti_cuts": 1,
 "finished": true,
 "final_int_nodes": 1,
 "master_time": 0.00465799999999994,
 "gap": 0.062499992187500984,
 "optimal": true,
 "build_time": 0.00043199999999998795,
 "sub_time": 0.0005439999999999889,
 "solve_time": 0.00481699999999996,
 "int_nodes": 2}
```

## Requirements

 * [CPLEX 12.7](https://www-01.ibm.com/software/websphere/products/optimization/cplex-studio-community-edition/)
 * Python 3.5
 * [Numpy](http://www.numpy.org/)

### Contributing

To contribute, I would appreciate if you respected the config in `setup.cfg`
which contains rules for formatting (using
[yapf](https://github.com/google/yapf)) and testing (using
[py.test](https://docs.pytest.org/en/latest/)).

## TODO

 - Return a parser directly from `benders/args.py`

Lots of other things! But most pressing right now would be parrallelisation.
