#
# This module will try to implement a small test example for Benders, taken
# from `http://www.ie.boun.edu.tr/~taskin/pdf/taskin_benders.pdf`, using an
# IRMT procedure where an iradiation has to be done using a set of rectangular
# apertures.
#
import cplex

from benders.master import Benders, BendersException, Report
from benders.sub import SubPb
from benders.utils import isZero, solValue, toKey
from benders.logger import Log
from benders.args import process_args, config_group


class SPTT(SubPb):
    """
    Primal sub-problem.
    """

    def __init__(self, apertures, radiation):
        nb_vars = len(apertures)
        xnames = ["x_{}".format(x) for x in range(nb_vars)]
        unames = []
        vnames = []
        self._id = 0

        self.cpx = cplex.Cplex()
        self.cpx.set_warning_stream(None)
        self.cpx.set_results_stream(None)
        self.cpx.set_log_stream(None)

        self.cpx.variables.add(obj=[1] * nb_vars, lb=[0] * nb_vars,
                               ub=[cplex.infinity] * nb_vars)
        self.cpx.variables.set_names(enumerate(xnames))

        # Turn off presolve
        self.cpx.parameters.preprocessing.presolve.set(0)
        self.cpx.parameters.lpmethod.set(
            self.cpx.parameters.lpmethod.values.dual)

        for i, rads in enumerate(radiation):
            for j, val in enumerate(rads):
                if val == 0:  # Skip clear areas
                    continue

                # TODO Find out why I *NEED* a SparsePair here.
                used = [
                    xnames[k] for k, ap in enumerate(apertures) if ap[i][j] > 0
                ]
                vals = [float(ap[i][j]) for ap in apertures if ap[i][j] > 0]
                expr = cplex.SparsePair(ind=used, val=vals)

                self.cpx.linear_constraints.add(lin_expr=[expr], senses=["E"],
                                                rhs=[val])

                unames.append("u_{}".format(len(unames)))

        self.us = unames

        self.rhs = []

        # Add the minimum irradiation
        for k, ap in enumerate(apertures):
            min_val = 10  # Max irradiation is 8 only

            # Save the RHS
            for i in range(len(ap)):
                for j in range(len(ap[i])):
                    if ap[i][j] == 1 and radiation[i][j] < min_val:
                        min_val = radiation[i][j]

            self.rhs.append(min_val)
            expr = cplex.SparsePair(ind=[xnames[k]], val=[1.0])

            self.cpx.linear_constraints.add(lin_expr=[expr], senses=["L"],
                                            rhs=[min_val])
            vnames.append("v_{}".format(k))

        self.vs = vnames
        self.cpx.linear_constraints.set_names(
            [(i, name) for i, name in enumerate(unames + vnames)])

        assert self.cpx.linear_constraints.get_num() == (
            len(unames) + len(vnames))
        assert self.cpx.linear_constraints.get_names() == unames + vnames

    def optiCut(self):
        """
        Return the duals for an optimality cut.
        """
        duals = self.cpx.solution.get_dual_values(self.vs)
        rhs = self.cpx.solution.get_dual_values(self.us)

        return duals, rhs

    def feasCut(self):
        """
        Return the duals for a feasibility cut.
        """
        # Use a Farkas certificate as we have the primal and not the dual; the
        # second parameter is the dual objective, should be equal to the $RHS *
        # coefs$.
        ray, _ = self.cpx.solution.advanced.dual_farkas()

        assert len(ray) == (len(self.us) + len(self.vs)), ray

        duals = [ray[i] for i in range(len(self.us), len(ray))]
        rhs = [ray[i] for i in range(0, len(self.us))]

        return duals, rhs

    def solve(self, solution):
        """
        Update the configuration and get the new solution.
        """
        for k, y in enumerate(solution):
            name = self.vs[k]

            if isZero(1 - y):
                self.cpx.linear_constraints.set_rhs(name, self.rhs[k])
            else:
                self.cpx.linear_constraints.set_rhs(name, 0)

        self.cpx.solve()

        return (
            self.cpx.solution.get_status() == self.cpx.solution.status.optimal)

    def value(self):
        """
        Value of the optimal solution found as it is the primal.
        """
        return self.cpx.solution.get_objective_value()

    def results(self):
        """
        Return a formatted version of the dual coefficients.
        """
        code = self.cpx.solution.get_status()

        if code == self.cpx.solution.status.optimal:
            Log.debug("Optimality Cut with x = {}".format(
                toKey(self.cpx.solution.get_values())))
            duals, rhs = self.optiCut()
        elif code == self.cpx.solution.status.infeasible:
            Log.debug("Feasibility Cut")
            duals, rhs = self.feasCut()
        else:
            raise BendersException(
                "Current sub solution is not valid: [{}] {}.".format(
                    code, self.solution.status[code]))

        lhs = [a * b for (a, b) in zip(duals, self.rhs)]
        mult = self.cpx.linear_constraints.get_rhs(self.us)

        Log.debug("Beta: {}, Alpha: {}".format(duals, rhs))
        Log.debug("LHS: {}, RHS: {}".format(lhs, solValue(rhs, mult)))

        return lhs, solValue(rhs, mult)


def main(args):
    """
    Create the model.
    """
    import logging
    import sys

    apertures = [[[1, 0], [0, 0]], [[0, 1], [0, 0]], [[0, 0], [1, 0]],
                 [[1, 0], [1, 0]], [[1, 1], [0, 0]]]
    radiation = [[8, 3], [5, 0]]
    intensity = 7

    bd = Benders()
    process_args(args, bd)

    bd.master.set_warning_stream(sys.stderr)
    bd.master.set_results_stream(sys.stderr)
    bd.master.set_log_stream(sys.stderr)
    bd.setMinimise()
    bd.addVars([intensity] * len(apertures),
               names=["y_{}".format(i) for i in range(len(apertures))])
    # No constraints in the base master
    bd.addSub({0: SPTT(apertures, radiation)})
    bd.setTimeLimit(60)
    bd.setMaxIters(10)

    bd.solve()

    return Report.json()


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    config_group(parser)
    print(main(parser.parse_args()))
