# -*- coding: utf-8 -*-
# Implement an example of the B&BC coming from Yossiri's thesis [1].
#
# [1] Adulyasak, Y. 2012.
#     Models and Solution Algorithms for Production Routing Problems.
#     HEC Montréal.
#
import cplex
import logging

from itertools import repeat
from math import ceil

from benders.master import Benders, Report, BendersException
from benders.sub import SubPb
from benders.utils import zipWith, solValue, toKey
from benders.logger import Log
from benders.args import process_args, config_group


class PSP(SubPb):
    """
    Primal sub-problem.
    """

    def __init__(self, integer=False):
        super(PSP, self).__init__()
        self._id = 0

        self.lnames = ["l{}".format(i) for i in range(1, 5)]
        self.ynames = ["y1", "y2"]
        self.xs = [15, 22]  # Value of the xs in the RHS

        if integer:
            vtypes = "BB"
        else:
            vtypes = "CC"

        self.add_vars(obj=[1, 2], lb=[0, 0], names=self.ynames, types=vtypes)

        # RHS will be modified when solving (l1)
        self.add_cstrs(lin_expr=[(self.ynames, [5, 8])], rhs=[0], senses="L",
                       names=[self.lnames[0]])
        # y value (l2)
        self.add_cstrs(lin_expr=[(self.ynames, [1, 1])], rhs=[1.5], senses="G",
                       names=[self.lnames[1]])
        # Upper bounds (l3-4)
        expr = [([self.ynames[0]], [1]), ([self.ynames[1]], [1])]  # y1, y2

        self.add_cstrs(lin_expr=expr, rhs=[2, 2], senses="LL",
                       names=self.lnames[2:])

        self.cpx.set_problem_type(self.cpx.problem_type.LP)

    def solve(self, solution):
        """
        Solve the problem using the master solution.
        """
        Log.debug("Use {} as solution.".format(solution))
        # Value of the master solution
        val = solValue(self.xs, solution)
        # Set as RHS of the first constraint
        self.cpx.linear_constraints.set_rhs(self.lnames[0], val)
        Log.debug("Set l0 to: {}".format(val))

        self.cpx.solve()

        opt = self.cpx.solution.get_status()

        return (opt == self.cpx.solution.status.optimal)

    def value(self):
        """
        Value of the optimal solution found as it is the primal.
        """
        return self.cpx.solution.get_objective_value()

    def optiCut(self):
        """
        Return the duals for an optimality cut.
        """
        duals = self.cpx.solution.get_dual_values()

        return duals[0], duals[1:]

    def feasCut(self):
        """
        Return the duals for a feasibility cut.
        """
        # Use a Farkas certificate as we have the primal and not the dual; the
        # second parameter is the dual objective, should be equal to the $RHS *
        # coefs$.
        ray, _ = self.cpx.solution.advanced.dual_farkas()

        assert len(ray) == len(self.lnames), ray

        return ray[0], ray[1:]

    def results(self):
        """
        Return a formatted version of the dual coefficients.
        """
        code = self.cpx.solution.get_status()

        if code == self.cpx.solution.status.optimal:
            Log.debug("Optimality Cut with y = {}".format(
                toKey(self.cpx.solution.get_values())))
            duals, rhs = self.optiCut()
        elif code == self.cpx.solution.status.infeasible:
            Log.debug("Feasibility Cut")
            duals, rhs = self.feasCut()
        else:
            raise BendersException(
                "Current sub solution is not valid: [{}] {}.".format(
                    code, self.cpx.solution.status[code]))

        Log.debug("l1: {}, l2-4: {}".format(duals, rhs))

        lhs = zipWith(repeat(duals), self.xs)
        mult = self.cpx.linear_constraints.get_rhs(self.lnames[1:])

        Log.debug("LHS: {}, RHS: {}".format(lhs, solValue(rhs, mult)))

        return lhs, solValue(rhs, mult)


class Round(PSP):
    """
    Upper bounding by rounding.
    """

    def __init__(self):
        super(Round, self).__init__()

    def solve(self, solution):
        """
        Get the fractional solution.
        """
        super(Round, self).solve(solution)
        self.solution = self.cpx.solution.get_values()

    def results(self):
        pass

    def value(self):
        """
        Round the fractional solution.
        """
        return sum(map(ceil, self.solution))


def main(args):
    bd = Benders()
    process_args(args, bd)

    bd.setMinimise()
    bd.addVars([6, 10], names=["x1", "x2"])
    bd.addSub({0: PSP()})
    bd.addHeur({0: Round()})
    bd.addIntSub({0: PSP(True)})

    bd.solve()

    bd.postprocessing()

    return Report.json()


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    config_group(parser)
    print(main(parser.parse_args()))
