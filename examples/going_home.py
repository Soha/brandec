from benders.master import Benders
from benders.sub import SubPb
from benders.logger import Log
from benders.report import Report
from benders.args import process_args, config_group


class Bus(SubPb):
    def __init__(self):
        """
        Create the bus trips.
        """
        SubPb.__init__(self)
        self.add_vars(obj=[80, 80, 80, 150, 250],
                      names=["y1p", "y2p", "y3p", "ypv", "y4v"])
        # For the first four constraints, we use positive coefs because:
        #  x - y = 0 <=> y = x
        expr = [(["y1p"], [1]), (["y2p"], [1]), (["y3p"], [1]), (["y4v"], [1]),
                (["y1p", "y2p", "y3p", "ypv"], [1, 1, 1, -1]),
                (["ypv", "y4v"], [1, 1])]

        self.add_cstrs(lin_expr=expr, rhs=[0, 0, 0, 0, 0, 1], senses="G" * 6)

    def solve(self, solution):
        for i, z in enumerate(solution):
            self.cpx.linear_constraints.set_rhs(i, z)

        self.write("bus")

        self.cpx.solve()

        return True

    def results(self):
        duals = self.cpx.solution.get_dual_values()
        self.lhs, self.rhs = duals[:4], duals[4:]

        print(self.lhs, self.rhs)

        return self.lhs, self.rhs[1]

    def value(self):
        return self.cpx.solution.get_objective_value()


def main(args):
    """
    Create the model.
    """
    planes = ["x({})".format(i + 1) for i in range(4)]

    bd = Benders()
    process_args(args, bd)

    bd.setMinimise()
    bd.addVars([100, 200, 200, 101], names=planes)
    bd.addConstrs([(planes, [1] * 4)], "E", [1])
    bd.addSub({0: Bus()})

    bd.master.write("planes.lp")

    bd.solve()

    return Report.json()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    config_group(parser)
    print(main(parser.parse_args()))
