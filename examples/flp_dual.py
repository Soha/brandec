#
# This module will try to implement a small test example for Benders,
# showcasing the use of the dual on a Facility location problem.
#
import cplex

from benders.master import Benders, BendersException, Report
from benders.sub import SubPb
from benders.utils import isZero, toKey
from benders.logger import Log
from benders.args import process_args, config_group


class FLP(SubPb):
    """
    Dual facility location sub-problem.
    """

    def __init__(self, costs):
        """
        Create a dual sub-problem using the delivery costs.  The model will
        have two variables: u, corresponding to fulfilling deliveries; v,
        corresponding to service availibility.

        Paramaters:

            - costs: an i*j matrix where i is the number of facilities and j
              the number of customers.
        """
        self._id = 0

        self.nb_i = len(costs)
        self.nb_j = len(costs[0])

        self.us = ["u({})".format(j) for j in range(self.nb_j)]
        self.vs = [["v({},{})".format(i, j) for j in range(self.nb_j)]
                   for i in range(self.nb_i)]

        self.cpx = cplex.Cplex()
        self.cpx.set_warning_stream(None)
        self.cpx.set_results_stream(None)
        self.cpx.set_log_stream(None)
        self.cpx.objective.set_sense(self.cpx.objective.sense.maximize)

        # Turn off presolve
        self.cpx.parameters.preprocessing.presolve.set(0)
        self.cpx.parameters.lpmethod.set(
            self.cpx.parameters.lpmethod.values.primal)

        # Add u variables
        self.cpx.variables.add(obj=[1] * self.nb_j, lb=[0] * self.nb_j,
                               ub=[cplex.infinity] * self.nb_j)

        # Add v variables
        self.cpx.variables.add(obj=[-1] * (self.nb_i * self.nb_j),
                               lb=[0] * (self.nb_i * self.nb_j),
                               ub=[cplex.infinity] * (self.nb_i * self.nb_j))

        # Set names
        vnames = [n for row in self.vs for n in row]  # Flatten
        self.cpx.variables.set_names(enumerate(self.us + vnames))

        # Add cost constraints
        for i, row in enumerate(costs):
            for j, c in enumerate(row):
                expr = ([self.us[j], self.vs[i][j]], [1, -1])

                self.cpx.linear_constraints.add(lin_expr=[expr], senses=["L"],
                                                rhs=[c])

    def optiCut(self):
        """
        Optimality cut dual values.
        """
        duals = [self.cpx.solution.get_values(vnames) for vnames in self.vs]
        rhs = self.cpx.solution.get_values(self.us)

        return duals, rhs

    def feasCut(self):
        """
        Feasibility cut from an extreme ray in an unbounded solution.
        """
        ray = self.cpx.solution.advanced.get_ray()

        rhs = [ray[i] for i in range(0, len(self.us))]

        duals = []
        for i in range(self.nb_i):
            offset = len(self.us) + i * self.nb_j
            end = offset + self.nb_j

            duals.append([ray[j] for j in range(offset, end)])

        return duals, rhs

    def results(self):
        """
        Return a formatted version of the dual coefficients.
        """
        code = self.cpx.solution.get_status()

        if code == self.cpx.solution.status.optimal:
            Log.debug("Optimality Cut with x = {}".format(
                toKey(self.cpx.solution.get_dual_values())))
            duals, rhs = self.optiCut()
        elif code == self.cpx.solution.status.unbounded:
            Log.debug("Feasibility Cut")
            duals, rhs = self.feasCut()
        else:
            raise BendersException(
                "Current sub solution is not valid: [{}] {}.".format(
                    code, self.solution.status[code]))

        # duals is a matrix where the sum of each row is the value we want
        lhs = [-sum(d) for d in duals]  # Put the sign back

        Log.debug("Us: {}, Vs: {}".format(duals, rhs))
        Log.debug("LHS: {}, RHS: {}".format(lhs, sum(rhs)))

        # Sum of u is enough
        return lhs, sum(rhs)

    def value(self):
        """
        Value of a solution.
        """
        return self.cpx.solution.get_objective_value()

    def solve(self, solution):
        """
        Solve the dual using the current master solution.
        """
        Log.debug(solution)

        for i, y in enumerate(solution):
            names = self.vs[i]

            if isZero(y):
                val = 0
            else:
                val = -1

            self.cpx.objective.set_linear(zip(names, [val] * len(names)))

        self.cpx.solve()

        return (
            self.cpx.solution.get_status() == self.cpx.solution.status.optimal)


def main(args):
    """
    Create the model.
    """
    import logging

    costs = [[2, 3, 4, 5, 7],
             [4, 3, 1, 2, 6],
             [5, 4, 2, 1, 3]]   # yapf: disable

    factory = [2, 3, 3]

    bd = Benders()
    process_args(args, bd)

    bd.setMinimise()
    bd.addVars(factory, names=["y_{}".format(i) for i in range(len(factory))])

    # No constraints in the base master
    bd.addSub({0: FLP(costs)})
    bd.setTimeLimit(60)
    bd.setMaxIters(10)

    bd.solve()

    return Report.json()


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    config_group(parser)
    print(main(parser.parse_args()))
