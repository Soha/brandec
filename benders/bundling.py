import random

from math import ceil
from benders.kmeans import k_means
from benders.utils import toKey


def cluster(scenarios, method, ratio):
    """
    Cluster the scenarios into cuts following a bundling method.

    Parameters:

        - scenarios: scenarios as binary lists w.r.t. master variables

        - method: bundling method

        - ratio: number of scenario per cut

    Return: Scenario bundles and mapping of scenarios to their clusters
    `(nb_cuts, {scenario_id: cluster_id})`
    """
    keys = list(scenarios.keys())

    if method == "one":
        nb_cuts = 1
        clusters = [keys]
        indices = {k: 0 for k in keys}

    if method == "all":
        nb_cuts = len(keys)
        clusters = keys
        indices = {k: k for k in keys}

    if method == "linear" or method == "random":
        nb_cuts = int(ceil(len(scenarios) / float(ratio)))
        clusters = []
        indices = {}
        size = len(scenarios)

        if method == "random":
            random.shuffle(keys)

        for n in range(0, size, ratio):
            c = len(clusters)
            ids = keys[n:n + ratio]

            clusters.append(ids)

            for i in ids:
                indices[i] = c

    if method == "kmeans":
        nb_cuts = int(ceil(len(scenarios) / float(ratio)))
        clusters = []
        indices = {}
        reverse = {toKey(s): k for k, s in scenarios.items()}

        # Number of clusters
        visits = list(scenarios.values())

        kmeans = k_means(visits, nb_cuts)

        for c, cluster in kmeans.items():
            bundle = []

            for s in cluster:
                s_id = reverse[toKey(s)]
                bundle.append(s_id)

            clusters.append(bundle)

        # Remove emtpy clusters
        nb_cuts = 0
        for c_id, cluster in enumerate([f for f in clusters if len(f) > 0]):
            nb_cuts += 1

            for s_id in cluster:
                indices[s_id] = c_id

    assert nb_cuts == len(set(indices.values())), "Too many cuts."
    assert set(keys) == set(indices.keys()), "Missing keys: {}".format(
        [i for i in keys if i not in indices])
    # TODO Missing keys occur when two scenarios are identical

    return nb_cuts, indices
