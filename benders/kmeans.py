#
# Implementation of the k-means++ clustering algorithm, which is k-means with
# better initialisation. The selection process is as follows:
#  1. Select an arbitrary centroid.
#  2. Select the next centroid using a weighted probability for the nodes.
#  3. Repeat untial we have k centroids.
#
# The clustering algorithm then proceeds as follows:
#  1. Label each node with the distance to its closest centroid.
#  2. Determine new centroids based on the labels of the nodes.
#
import numpy as np

from random import choice


def hamming_distance(s1, s2):
    """
    Return the Hamming distance between equal-length sequences
    """
    if len(s1) != len(s2):
        raise ValueError("Undefined for sequences of unequal length")

    return sum(el1 != el2 for el1, el2 in zip(s1, s2))


def cluster_points(elements, centroids, k):
    """
    Assign elements to their closest centroid.
    """
    clusters = {i: [] for i in range(k)}

    for el in elements:
        # Find the closest centroid
        dist = float('inf')
        best = None

        # Find the closest centroid
        # TODO pythonise this
        for i, x in enumerate(centroids):
            ham = hamming_distance(el, x)

            if ham < dist:
                best = i
                dist = ham

        if best is None:
            print("No centroid found!")
        else:
            clusters[best].append(el)

    return clusters


def new_centroids(elements, clusters, k):
    """
    Reevaluate the centroids with the updated clusters.
    """
    new = []

    for i in range(k):
        cluster = clusters[i]

        if len(cluster) == 0:
            centroid = choice(elements)
        else:
            centroid = np.mean(clusters[i], axis=0)

        new.append(centroid)

    return new


def first_centroids(elements, k):
    """
    Kmeans++ initialisation method, instead of selecting `k` centroids at
    random, we use an iterative approach:

        1. Choose the first centroid at random.

        2. For each element, compute its distance and the nearest centroid:
           D(x).

        3. Choose a new centroid using a weighted distribution where elements
           have a D(x)^2 probability.

    Repeat until `k` centroids have been selected.

    Parameters:

        - elements: elements to be clustered

        - k: number of clusters

    Return: A list of centroids
    """
    # Initial centroid
    size = len(elements)
    centroids = [choice(elements)]
    probas = np.array([0.0] * size)

    while len(centroids) < k:
        for i, el in enumerate(elements):
            if el in centroids:
                probas[i] = 0.0
            else:
                dist = min([hamming_distance(el, c) for c in centroids])

                probas[i] = float(dist * dist)

        total = sum(probas)
        # Weighted distribution
        index = np.random.choice(size, p=probas / total)

        centroids.append(elements[index])

    return centroids


def k_means(elements, k):
    """
    K-means algorithm using Hamming distance.  Starts by electing `k` centroids
    at random, then iterates over two steps until convergence.

        1. Label each node with the distance to its closest centroid.

        2. Determine new centroids based on the labels of the nodes.

    Parameters:

        - elements: elements to cluster

        - k: number of clusters to define
    """
    # Find the first k centroids
    centroids = first_centroids(elements, k)
    previous = []
    i = 0

    # TODO Change max iterations to a parameter?
    while not np.array_equal(centroids, previous) and i < 100:
        previous = centroids
        # Label points with their closest cluster
        clusters = cluster_points(elements, centroids, k)
        # Find the new centroids
        centroids = new_centroids(elements, clusters, k)
        i += 1

    return clusters
