import json


class Reporter():
    """
    Class to save data used in the report(s).  Implement json serializing, json
    file writing; can be extended.
    """

    def __init__(self):
        """
        Create the default fields for Benders's statistics.
        """
        # Timings
        self.build_time = 0
        self.upper_time = 0
        self.sub_time = 0
        self.int_time = 0
        self.master_time = 0
        self.solve_time = 0

        # Other data
        self.finished = False  # Closed the B&C tree
        self.optimal = False  # Global optimal found
        self.cutoff = False   # Early cut-off
        self.nb_nodes = 0  # Number of nodes explored in the B&C tree
        self.int_nodes = 0  # Number that were integer
        self.final_int_nodes = 0  # Number of open solutions after the B&C tree

        # Cuts
        self.nb_opti_cuts = 0
        self.nb_feas_cuts = 0
        self.nb_L_shaped = 0
        self.nb_H_shaped = 0

    def json(self, filename=None):
        """
        Serialize the current report as a JSON object, if a filename is
        provided, write the (pretty printed) object to the file; otherwise
        return a string representation.

        Parameters:

            - filename: (optional) name of the file to write the report to
        """
        if filename is None:
            return json.dumps(self.__dict__)
        else:
            with open(filename, 'w') as out:
                json.dump(self.__dict__, out, indent=2)

    def add(self, values):
        """
        Add fields to the report.

        Parameters:

            - values: a key/value dictionary

        >>> r = Reporter()
        >>> r.add({"test": True})
        >>> r.test
        True
        """
        for k, v in values.items():
            setattr(self, k, v)

    def __repr__(self):
        return str(self.__dict__)


# Make a global class so as to have all data in the same place
Report = Reporter()
