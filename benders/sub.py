import cplex
from abc import ABCMeta, abstractmethod
from benders.utils import toKey


class SubPb(metaclass=ABCMeta):
    """
    Class which will be called as either a lower or upper bounding procedure.
    """
    # Global ratio, as in a single problem will need a single ratio
    ratio = 1

    @staticmethod
    def set_ratio(ratio):
        SubPb.ratio = ratio

    def __init__(self, sid=0):
        self._id = sid

        self.cpx = cplex.Cplex()
        self.cpx.set_warning_stream(None)
        self.cpx.set_results_stream(None)
        self.cpx.set_log_stream(None)

        self.cpx.parameters.threads.set(1)
        self.cpx.parameters.preprocessing.reduce.set(0)
        self.cpx.parameters.preprocessing.presolve.set(0)
        self.cpx.parameters.lpmethod.set(
            self.cpx.parameters.lpmethod.values.dual)

    @abstractmethod
    def solve(self, solution, data):
        """
        Base method to be called by deriving classes, calls the optimisation
        process and returns if the solution is optimal.
        """
        pass

    @abstractmethod
    def value(self):
        """
        Return the current value of the sub-problem.
        """
        pass

    @abstractmethod
    def results(self):
        """
        Return the dual costs and the RHS of the current sub-problem as a
        ([duals], rhs) t-uple.
        """
        pass

    def getId(self):
        """
        Return the id of the current object, at least have the deriving classes
        define an `_id` field.
        """
        return self._id

    @staticmethod
    def _add_members(which, names, **kwargs):
        idx = which.add(**kwargs)

        if names:
            which.set_names(zip(idx, names))

        return idx

    def add_vars(self, names=[], **kwargs):
        return self._add_members(self.cpx.variables, names, **kwargs)

    def add_cstrs(self, names=[], **kwargs):
        return self._add_members(self.cpx.linear_constraints, names, **kwargs)

    def write(self, name=None):
        if name is None:
            f = "{}.lp".format(self._id)
        else:
            f = "{}.lp".format(name)

        self.cpx.write(f)

    def objective(self):
        """
        "Actual" objective value, without ratio.
        """
        return self.cpx.solution.get_objective_value()


class SubPareto(SubPb):
    """
    Class implementing a Pareto sub-problem process: takes a regular problem
    and a Pareto counter part and run the calculations.
    """
    core = None

    @staticmethod
    def set_core(core):
        SubPareto.core = core

    def __init__(self, regular, pareto, sid=0):
        self.regular = regular
        self.pareto = pareto

        self._id = sid
        self.pid = None

    def _add_pareto(self, solution, value):
        """
        Add/update the pareto constraint by using the current master solution
        and the solution to the regular sub-problem.  Not an abstract method as
        this is a hybrid class.

        Parameters:

            - solution: current master solution

            - value: value of the regular sub-problem

        Return: id of the new constraint
        """
        raise NotImplementedError("Must be overriden in children")

    def solve(self, solution):
        """
        TODO: Check what happens with an infeasible regular sub-problem.
        """
        first = self.regular.solve(solution)

        if first:
            # Have to get the *actual* objective to pass on to the Pareto
            primal_val = self.regular.objective()

            if self.pid is not None:
                self.pareto.cpx.linear_constraints.delete(self.pid)

            self.pid = self.pareto._add_pareto(solution, primal_val)

            return self.pareto._solve()

        return False

    def value(self):
        return self.regular.value()

    def results(self):
        """
        Results are directly from the Pareto sub-problem.
        """
        return self.pareto.results()
