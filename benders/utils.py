#
# Utilities for Benders
#
import operator as op
import numpy as np


class Epsilon():
    """
    Class holding the tolerance parameter.
    """
    def __init__(self, e=1e-6):
        self.e = e

    def setE(self, e):
        self.e = e

    def __eq__(self, other):
        return self.e == other

    def __le__(self, other):
        return self.e <= other

    def __gt__(self, other):
        return self.e > other

    def __lt__(self, other):
        return self.e < other

    def __ge__(self, other):
        return self.e >= other

    def __sub__(self, other):
        return self.e - other

    def __rsub__(self, other):
        return other - self.e

    def __add__(self, other):
        return self.e + other

    def __radd__(self, other):
        return self.e + other

    def __abs__(self):
        return self.e


epsilon = Epsilon()


def trim(x, precision=6):
    """
    Trim results to use with CPLEX's precision.

    Parameters:

        - x: list/array of numbers to be rounded to a given precision

        - precision: number of digits after decimal point

    >>> trim([.101, .22], precision=2)
    [0.1,  0.22]

    Verify that the conversion to zero works
    >>> trim([0, 0])
    [0.0, 0.0]
    """
    return np.around(x, decimals=precision).astype(float).tolist()


def zipWith(list_a, list_b, function=op.mul):
    """
    Zip two lists together using the given function, defaults to using a
    product.

    >>> zipWith([1, 2, 3], [4, 5, 6])
    [4, 10, 18]

    >>> zipWith([1, 2, 3], [4, 5, 6], function=op.pow)
    [1, 32, 729]
    """
    return [function(a, b) for a, b in zip(list_a, list_b)]


def toKey(solution, half=False):
    """
    "Hash" function for master solutions.

    >>> toKey([1, 0, 0, 0, 1, 0, 1, 0])
    '1,0,0,0,1,0,1,0'

    >>> toKey([1 - epsilon, epsilon, 0, 0, 0.9, 0, 1, 0])
    '1,0,0,0,1,0,1,0'

    >>> toKey([1 - epsilon, epsilon, 0, 0, 0.9, 0, 1, 0], half=True)
    '1,0,0,0,-,0,1,0'

    >>> toKey([1, 2, 3, 4 + epsilon])
    '1,2,3,4'

    TODO: Should we validate the input?
    """
    key = ""

    for x in solution:
        if isZero(x):
            key += '0'
        else:
            up = round(x)
            lo = float(x)

            if isZero(up - lo) or not half:
                key += str(up)
            else:
                key += '-'

    return ",".join(key)        # Intersperse hack


def toSol(key):
    """
    Turn a hashed solution back into a list of values.

    >>> toSol("1,0,0,0,1,0,1,0")
    [1, 0, 0, 0, 1, 0, 1, 0]

    >>> toSol("32,57,3")
    [32, 57, 3]
    """
    return list(map(int, key.split(",")))


def solValue(solution, coefficients):
    """
    The value of a solution is: $c^T \dot \bar{z}$, that is the
    coefficients of the variables times their current values.

    >>> solValue([1, 2, 3], [4, 5, 6])
    32
    """
    return sum(zipWith(solution, coefficients))


def isZero(value):
    """
    Checks for almost zero equality to account for rounding errors.

    >>> isZero(1 - (1 - epsilon))
    True

    >>> isZero(epsilon)
    True
    """
    # Without trim(): 1 - (1 - eps) > eps
    return trim(abs(value)) <= epsilon


def mipGap(best_int, best_bound):
    return abs(best_bound - best_int) / (epsilon + abs(best_int))


def getGap(qs, values):
    """
    Get the average gap between lists of values.
    """
    gaps = zipWith(qs, values, mipGap)
    return sum(gaps) / len(gaps)
