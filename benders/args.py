import logging
import random
from benders.logger import Log
from benders.utils import epsilon
from benders.report import Report


def config_group(parser):
    """
    Add standard configuration options for Benders.
    """
    config_group = parser.add_argument_group('Benders Configuration')

    config_group.add_argument("--output", type=str,
                              help="Output for CPLEX data.")
    config_group.add_argument("--time", type=int,
                              help="Time limit in seconds.")
    config_group.add_argument("--itrs", type=int,
                              help="Maximum number of iterations.")
    config_group.add_argument("--pareto", action='store_true',
                              help="Use a Pareto sub-problem.")
    config_group.add_argument(
        "--no-heur", action='store_true',
        help="De-activate CPLEX heuristics")
    config_group.add_argument(
        "--exact-heur", action='store_true',
        help="Enable exact heuristic cut-offs")
    config_group.add_argument("--subopt", type=int,
                              help="Suboptimality value in %.")
    config_group.add_argument("--h-shaped", action="store_true",
                              help="Enable cardinality (H-shaped) cuts.")
    config_group.add_argument("--ratio", type=int, default=1,
                              help="Number of scenario per cut.")
    config_group.add_argument(
        "--bundling", type=str, default="one",
        choices=["one", "all", "linear", "random", "kmeans"], help="\n".join([
            "Bundling scheme, a * means it uses a ratio:",
            " - one: single cut;", " - all: one cut per scenario;",
            " * linear: aggregate in a linear fashion;",
            " * random: same but shuffle before aggregation;",
            " * kmeans: use k-means++ to cluster scenarios."
        ]))
    config_group.add_argument("--seed", type=int, default=5630267,
                              help="RNG's seed, if 0 no control")
    config_group.add_argument('--verbose', '-v', action='count', default=0,
                              help="Verbosity, None, -v, or -vv")
    config_group.add_argument("--epsilon", '-e', type=float,
                              help="Tolerance value for results.")
    config_group.add_argument("--debug", action='store_true',
                              help="Enable debug during the master B&B.")
    config_group.add_argument("--save-vars", action='store_true',
                              help="Enable saving variables during the B&C.")


def process_args(args, bd, set_seed=random.seed):
    """
    Process the standard options.

    Parameters:

        - args: results of `parse_args()`

        - bd: current Benders object

        - set_seed: chosen RNG source (e.g., numpy.random.seed or random.seed)
    """
    if args.seed > 0:
        set_seed(args.seed)

    if args.verbose == 0:
        level = logging.WARNING
    elif args.verbose == 1:
        level = logging.INFO
    else:
        level = logging.DEBUG

    # Have to have both because reasons
    logging.basicConfig(level=level)
    Log.setLevel(level)

    if args.time is not None:
        bd.setTimeLimit(args.time)

    if args.itrs is not None:
        bd.setMaxIters(args.itrs)

    if args.epsilon is not None:
        assert args.epsilon > 0, "--epsilon needs to be positive."

        if args.epsilon > 1:
            Log.info("Large values of epsilon are not recommended.")

        epsilon.setE(args.epsilon)

    if args.output is not None:
        bd.setCPLEXOutput(args.output)

    if args.debug:
        bd.enableDebug()

    if args.no_heur:
        bd.noHeur()

    if args.exact_heur:
        bd.isExactHeur()

    if args.h_shaped:
        bd.enableHShaped()

    if args.save_vars:
        bd.enableSaveVars()

    if args.subopt is not None:
        assert args.subopt > 0, "--subopt must be positive."
        bd.setSubopt(args.subopt)

    Report.config = vars(args)
