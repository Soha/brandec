from collections import OrderedDict
from copy import deepcopy

import operator
import cplex
import cplex.callbacks as cb

from benders.report import Report
from benders.logger import Log
from benders.timer import Timer
from benders.utils import isZero, toKey, toSol, solValue, trim, mipGap, getGap
from benders.utils import zipWith


class BendersException(Exception):
    pass


class Benders():
    """
    Branch-and-Benders-Cut main class.  A container for a CPLEX solver where
    branching will have callbacks with special operations on integer nodes.

    It is advised you use the provided methods, but in case, this is Python,
    everything is public.
    """
    # List of explored solutions, stored as:
    #   {"key": (base, lp_relax, heuristic)}
    solutions = OrderedDict()
    values = OrderedDict()  # Values of the master variables during search
    save_vars = False
    iterations = 0
    max_iterations = None
    time_limit = None
    exact_heur = False
    L_shaped = None
    H_shaped = False
    subopt = 0.0

    # Inner containers
    master_vars = []
    aux_vars = []
    ext_vars = []
    sub_pbs = None
    int_subs = None
    heuristic = None
    cuts = None
    cut_vars = None
    bundles = None
    cut_sense = None

    # Callback data
    new_incumbent = []
    last_cuts = []

    # Output
    sep = None

    # Debug
    debug = False

    @staticmethod
    def exception(ex):
        raise BendersException(ex)

    @staticmethod
    def make_header(entries):
        entry = "{:^10s}"
        header = [" Itrs"]

        for e in entries:
            header.append(entry.format(e))

        if Benders.sep is None:
            Benders.sep = " + ".join([" ----"] + ['-' * 10] * len(entries))

        Log.info(" | ".join(header))
        Log.info(Benders.sep)

    @staticmethod
    def progress(itrs, master_value, incumbent, relax_bound, gap, opti=True,
                 heur_bound=None, post=False):
        """
        Print the progress.

        TODO: variable output depending on the configuration
        """
        num = "{:10.3f}"
        void = "{:>10s}".format("-")
        line = [" {:4d}".format(itrs)]
        line.append(num.format(master_value))
        line.append(num.format(incumbent))

        if relax_bound is None:
            line.append(void)
        else:
            line.append(num.format(relax_bound))

        if post:
            if gap:
                line.append("{:^10s}".format('X'))
            else:
                line.append(num.format(master_value + relax_bound))
        else:
            if opti:
                line.append("{:10.2%}".format(gap))
            else:
                line.append(void)

        if Benders.heuristic is not None:
            if heur_bound is None:
                line.append(void)
            else:
                line.append(num.format(heur_bound))

        # Itrs | Master | q | q(z) | Opt | UB
        Log.info(" | ".join(line))

    def _check_time(self, time):
        if self.time_limit is None:
            return False

        if time > self.time_limit:
            Log.info("Maximum time reached")

            Report.optimal = False

            return True

        return False

    def __init__(self):
        # Optimisation status
        self.finished = False  # Completion of the B&C tree
        self.optimal = False  # Found global optimum

        # Default CPLEX config
        self.master = cplex.Cplex()
        self.master.set_warning_stream(None)
        self.master.set_results_stream(None)
        self.master.set_log_stream(None)
        # We keep the error stream as it is usually important

    @staticmethod
    def enableDebug():
        """
        Enable debugging.
        """
        Benders.debug = True
        Benders.save_vars = True

    @staticmethod
    def enableSaveVars():
        Benders.save_vars = True

    @staticmethod
    def enableHShaped():
        Benders.H_shaped = True

    def noHeur(self):
        """
        Disable CPLEX MIP heuristics.
        """
        self.master.parameters.mip.strategy.heuristicfreq.set(-1)
        self.master.parameters.mip.strategy.fpheur.set(-1)
        self.master.parameters.mip.strategy.rinsheur.set(-1)

    @staticmethod
    def isExactHeur():
        """
        Enable exact heuristic switch: allow cut-off in the B&B tree and in the
        postprocessing phase.
        """
        Benders.exact_heur = True

    def is_better(self, new, base, default=True, subopt=False):
        """
        Parameters:

            - new: value to check

            - base: previous value

            - default: what to return when both values are too close

            - subopt: use suboptimality check

        Return: if the new value *is better* (lower for min, higher for max)
        """
        if isZero(new - base):
            return default or subopt

        if subopt:
            new *= 1 + Benders.subopt

        if self.is_min:
            return new < base
        else:
            return base < new

    def finish(self, message, early=False):
        """
        Finish optimisation.
        """
        Report.cutoff = early
        Report.nb_nodes = self.get_num_nodes()

        Log.info(message)

        if self.int_subs is None:
            Log.info("Stop after {} nodes ({} integer).".format(
                self.get_num_nodes(), self.iterations))

        self.abort()

    def setCPLEXOutput(self, filename="cplex.log"):
        """
        Set the output of CPLEX to the given file.

        Parameters:

            - filename: output for CPLEX streams, default `cplex.log`
        """
        self.master.set_warning_stream(filename)
        self.master.set_results_stream(filename)
        self.master.set_log_stream(filename)

    def setMaximise(self):
        self.master.objective.set_sense(self.master.objective.sense.maximize)
        Benders.cut_sense = "G"
        Benders.is_min = False
        Benders.heur_bound = -float('inf')
        Benders.relax_bound = -float('inf')

    def setMinimise(self):
        self.master.objective.set_sense(self.master.objective.sense.minimize)
        Benders.cut_sense = "L"
        Benders.is_min = True
        Benders.heur_bound = float('inf')
        Benders.relax_bound = float('inf')

    def setMaxIters(self, its):
        """
        Define the maximum number of (Benders) iterations to perform, do note
        this is **not** the number of nodes explored but the number of integer
        nodes processed.
        """
        if its > 0:
            Benders.max_iterations = its
        else:
            self.exception("Negative number of iterations given.")

    def setSubopt(self, pct=None, val=None):
        """
        Set the suboptimality gap for the search.

        Parameters:

            - pct: Integer value for the tolerance (e.g., 10 <=> 10%)

            - val: Direct value
        """
        assert (pct is None) != (val is None),\
            "Can only have either pct (={}) or val (={}) set.".format(pct, val)
        if pct is not None:
            Benders.subopt = pct / 100
        else:
            Benders.subopt = val

    def addVars(self, objs, lbs=[], ubs=[], names=[], auxiliary=False,
                extended=False, vtype="B"):
        """
        Add variables to the master problems, these should be integer (binary,
        really) and we will branch an them.  Incumbent variables will be added
        later.

        Parameters:

            - obj: list of objective coefficients

            - lb: list of lower bounds

            - ub: list of upper bounds

            - names: name of the variables in the order submitted

            - auxiliary: master variables not involved in cuts.

            - extended: variables involved neither in cuts nor in the objective.

            - vtype: type of the variables, can be either one entry or as many
              as the variables.

        Note: The lbs, ubs, and names can be omitted.
        """
        if auxiliary and extended:
            Log.warning("Cannot have both auxiliary and extended, will "
                        "default to auxiliary")

        if extended and any([x > 0 for x in objs]):
            Log.error("Extended variables must have nil objective coefficients,"
                      " use auxiliary instead.")

        nb_vars = len(objs)

        if auxiliary and "C" in vtype:
            Log.error("Cannot have continuous auxiliary variables.")

        with Timer() as t:
            types = [vtype] * nb_vars if len(vtype) == 1 else vtype

            # Apparently batching without names is faster
            idx = self.master.variables.add(obj=objs, lb=lbs, ub=ubs,
                                            types=types)

            if len(names) > 0:
                if len(names) != nb_vars:
                    # Not sure CPLEX handles this by default
                    self.exception("Names size does not match number of vars.")
            else:
                names = ["z({})".format(i) for i in range(nb_vars)]

            self.master.variables.set_names(zip(idx, names))

            # We have three types of variables:
            #  - master :: involved in the objective and the subproblem;
            #  - auxiliary :: involved only in the objective; and,
            #  - extended :: involved only in the constraints.
            if auxiliary:
                self.aux_vars.extend(names)
            elif extended:
                self.ext_vars.extend(names)
            else:
                self.master_vars.extend(names)

        Report.build_time += t.interval

        Log.info("Added {} master variables.".format(len(objs)))

    def addAuxVars(self, objs, lbs=[], ubs=[], names=[], vtype="B"):
        """ Shorthand to add auxiliary variables. """
        self.addVars(objs, lbs=lbs, ubs=ubs, names=names, auxiliary=True,
                     vtype=vtype)

    def addExtVars(self, names, lbs=[], ubs=[], vtype="B"):
        """
        Shorthand to add extended variables, we force the `names` parameter
        because the objective coefficients will be nil.
        """
        self.addVars([0] * len(names), lbs=lbs, ubs=ubs, names=names,
                     vtype=vtype, extended=True)

    def addConstrs(self, expr, senses, rhs, names=[]):
        """
        Add constraints to the master problem.

        Parameters:

            - expr: list of (variable_id, coef), or use SparsePair

            - senses: constraints senses

            - rhs: constraints right-hand sides

            - names: name given to the constraints as a list
        """
        with Timer() as t:
            self.master.linear_constraints.add(lin_expr=expr, senses=senses,
                                               rhs=rhs)

            if len(names) > 0:
                if len(expr) != len(names):
                    self.exception(
                        "Names size does not match number of constraints.")

                self.master.linear_constraints.set_names(enumerate(names))

        Report.build_time += t.interval

        Log.info("Added {} constraints.".format(
            self.master.linear_constraints.get_num()))

    def setCuts(self, cuts, lb=0):
        """ Set the cut container and create the incumbent variables. """
        Benders.cuts = cuts
        # Default to single cut

        size = len(Benders.cuts)
        # Add incumbent variables
        Benders.cut_vars = OrderedDict((cut_id, "q_{}".format(cut_id))
                                        for cut_id in sorted(Benders.cuts))

        objs = [1.0] * size

        self.master.variables.add(
            obj=objs, names=Benders.cut_vars.values(), lb=[lb] * size)

    def addSub(self, sub_pbs, cuts={}, lb=0):
        """
        Add sub-problems in dictionary form, you have to provide a dictionary
        for cut bundling -- in case of a single cut, skip this parameter.

        Parameters:

            - sub_pbs: {sub_id: sub-problem}

            - cuts: {sub_id: cut_id}
        """
        with Timer() as t:
            Benders.sub_pbs = sub_pbs

            # Default to single cut
            if len(cuts) == 0:
                cut_ids = {0: [s.getId() for s in sub_pbs.values()]}
                Benders.bundles = {sub_id: 0 for sub_id in sub_pbs}
            else:
                Benders.bundles = cuts
                cut_ids = {cut_id: [] for cut_id in cuts.values()}

                for sub_id, cut_id in cuts.items():
                    assert sub_id in Benders.sub_pbs, \
                        "Unknown sub-pb {} in cut bundle {}.".format(
                            sub_id, cut_id)

                    cut_ids[cut_id].append(sub_id)

            self.setCuts(cut_ids, lb=lb)

        Report.build_time += t.interval

        Log.info("Registered {} sub-problems bundled in {} cuts.".format(
            len(self.bundles), len(self.cuts)))

    def addIntSub(self, int_subs):
        """
        Add integer sub-problems in dictionary form, they do not have to
        respect cuts or anything.

        Parameters:

            - int_subs: {sub_id: sub-problem}
        """
        with Timer() as t:
            Benders.int_subs = int_subs

        Report.build_time += t.interval

        Log.info("Registered {} integer sub-problems.".format(
            len(Benders.int_subs)))

    def addHeur(self, heur_pbs, exact=False):
        """
        Add heuristic bounding problem, must implement an `solve()` method, and
        register appropriate callbacks.

        Parameters:

            - heur_pbs: collection of heuristic bounding function based on
              sub-problems' ids
        """
        Benders.heuristic = heur_pbs
        Benders.exact_heur = exact

        self.master.register_callback(Prune)
        self.master.register_callback(Reject)
        self.master.register_callback(Heuristic)

    def setTimeLimit(self, time):
        self.master.parameters.timelimit.set(time)
        self.time_limit = time

    def addLShaped(self, values):
        """
        Enable L-shaped cuts by passing their lowerd bounds.

        Parameters:

            - values: single value (global) or per-cut values as dict.
        """
        Benders.L_shaped = values

    def solve(self):
        """
        Trigger the solving process.
        """
        if Benders.master_vars is None:
            self.exception("No master variables added.")

        if Benders.sub_pbs is None:
            self.exception("No sub-problem registered")

        if Benders.heuristic is not None:
            # Gap between UB and LB
            Report.int_gap = 0

            assert Benders.heuristic.keys() == self.sub_pbs.keys(), \
                "Heuristic and sub-pbs need to match."

        if Benders.L_shaped is not None and type(Benders.L_shaped) is dict:
            assert Benders.L_shaped.keys() == self.cuts.keys(), \
                "L-shaped cuts need to match the bundling scheme."

        if Benders.H_shaped:
            assert Benders.heuristic is not None, \
                "H-shaped cuts only work with integer sub-problems."

            assert Benders.L_shaped is not None, \
                "H-shaped cuts need a global lower bound (L-shaped)."

            self.master.register_callback(Cardinal)

        if Benders.int_subs is not None:
            assert Benders.int_subs.keys() == self.sub_pbs.keys(), \
                "Int subs and sub-pbs need to match."

        if Benders.cut_sense is None:
            self.exception("No optimisation sense defined")

        if self.time_limit is None:
            Log.info("No time limit defined.")

        if Benders.max_iterations is None:
            Log.info("No maximum number of iterations")

        self.master.register_callback(Cuts)

        header = ["Master", "q", "q(z)", "Gap"]

        if Benders.heuristic is not None:
            header.pop()
            header.extend(["Int. Gap", "UB"])

        Benders.make_header(header)

        with Timer() as t:
            self.master.solve()

        # "Wall clock" time, solver time can be accessed within CPLEX
        Report.solve_time = t.interval
        Report.master_time = t.interval

        if Benders.max_iterations is None:
            self.finished = True
        else:
            self.finished = Benders.iterations < Benders.max_iterations

        if self.finished and Benders.time_limit is not None:
            self.finished = Report.solve_time < Benders.time_limit

        Report.finished = self.finished
        Report.int_nodes = Benders.iterations

        if self.heuristic is not None:
            Report.gap = mipGap(Benders.heur_bound, Benders.relax_bound)
            Report.int_gap /= Benders.iterations
        else:
            # Instead of using the CPLEX solution, we can rely on the last
            # explored solution in this case because, without UB, solution
            # increases monotonically.
            Report.optimal = self.finished

            # Access last key added
            Log.info("Best solution found ({}), using: {}".format(
                self.relax_bound, next(reversed(self.solutions))))

    def _solve_int_subs(self, solution):
        value = 0

        for sub_id, sub_int in self.int_subs.items():
            with Timer() as t:
                try:
                    sub_int.solve(solution)
                    value += sub_int.value()
                except cplex.exceptions.errors.CplexSolverError as ex:
                    Log.warning("Could not solve {} with {}".format(
                        sub_id, solution))
                    raise ex

            Report.int_time += t.interval

            if self._check_time(Report.solve_time + Report.int_time):
                Log.info("Maximum time reached.")
                return None

        return value

    def _process_int_subs(self, remaining):
        if self.int_subs is None:
            self.exception("No integer sub-problem registered")

        nodes = OrderedDict(sorted(remaining.items(), key=lambda t: t[1][1]))

        Log.info(Benders.sep)
        Benders.make_header(["Master", "LP", "Int. Value", "Objective", "Best"])

        if self.is_min:
            heur_bound = float("inf")
        else:
            heur_bound = -float("inf")

        n = 0  # Nb. of solution *tested*
        best_sol = None

        for sol, data in nodes.items():
            opt = False
            n += 1
            # Filter auxiliary variables
            solution = toSol(sol)[:len(self.master_vars)]
            master_val, lp_relax, _ = data
            objective = master_val + lp_relax

            # Verify that the linear relaxation is still a better relaxed
            # bound than the current best integer solution
            if self.is_better(heur_bound, objective, subopt=True):
                continue

            value = self._solve_int_subs(solution)
            self.solved.append(sol)

            if value is None:  # Timeout
                self.finished = False
                break

            # Update heuristic bound using integer solution
            if self.is_better(value + master_val, heur_bound):
                heur_bound = value + master_val
                best_sol = sol
                opt = True

            self.progress(n, master_val, lp_relax, value, opt,
                          heur_bound=heur_bound, post=True)

        return best_sol, heur_bound

    def postprocessing(self):
        """
        Default post-processing is solving the open nodes after the B&B tree.
        Currently separated from `solve()` to let users do something
        in-between.

        Note: Three-Phase Benders is determined by the absence of heuristic
        subs.
        """
        if self.heuristic is None:
            # 3DB uses the last integer solution found
            remaining = Benders.solutions.popitem(last=True)
        else:
            remaining = {
                k: v
                for k, v in Benders.solutions.items()
                if (v[1] is not None and
                    self.is_better(v[0] + v[1], self.heur_bound))
            }

        # TODO can we have cases with 0 available solutions?
        if len(remaining) == 0:
            Log.info(Benders.solutions)
            self.exception("No candidate solution to solve")

        self.solved = []  # Index of solutions *solved*

        # In case we have an exact heuristic, we can simply sort the list of
        # open solutions and return the best.
        if Benders.exact_heur:
            final = sorted(remaining.items(), key=lambda t: t[1][0] + t[1][2])[0]
            best_sol = final[0]
            heur_bound = final[1][0] + final[1][2]
        else:
            best_sol, heur_bound = self._process_int_subs(remaining)

        Log.info("Best solution found ({}), using: {}".format(
            heur_bound, best_sol))

        # We can only guarantee optimality if we finished the B&B tree
        self.optimal = self.finished

        Report.solve_time += Report.int_time
        Report.final_int_nodes = len(self.solved)
        Report.solution = heur_bound
        Report.optimal = self.optimal

        return toSol(best_sol)


class BendersCut(cb.MIPCallback, Benders):
    """
    Generic class to gather cut functions for Benders callbacks. We need that as
    our version of CPLEX does not have generic callbacks.
    """

    @staticmethod
    def _verify_cuts(cuts, solution, relax_bounds):
        """
        Verify that the cuts generated are valid.
        """
        for cut_id, cut in cuts.items():
            cut_value = solValue(cut["duals"], solution)
            sub_objective = relax_bounds[cut_id]

            assert isZero(cut_value + cut["rhs"] - sub_objective), \
                "[{}] Cut != obj: {} + {} =/= {}".format(
                    cut_id, cut_value, cut["rhs"], sub_objective)

    def _verify_incumbent(self, names, relax_bounds):
        """
        Verify that the incumbent variables is always a relaxed bound on the
        sub-problems' values.
        """
        incumbents = self.get_values(list(self.cut_vars.values()))
        for i, (key, name) in enumerate(names.items()):
            if self.is_better(relax_bounds[key], incumbents[i], default=False):
                raise BendersException(
                    "Incumbent {} ({}) exceeds relaxed bound: {} {}".format(
                        i, key, incumbents[i], relax_bounds[key]))

    @staticmethod
    def _verify_sol(cuts, solution):
        for cut in cuts.values():
            assert solValue(cut["duals"], solution) - cut["rhs"] < 0, \
                "Cut does not remove current solution"

    def get_last_cuts(self):
        if not self.last_cuts:
            self.compute_cuts()

        return self.last_cuts

    def get_obj_vals(self):
        """
        Get the components of a Benders solution: value of the master variables
        and the auxiliary variables.
        """
        if self.aux_vars:
            xvals = self.get_values(self.aux_vars)
            xcoefs = self.get_objective_coefficients(self.aux_vars)
            aux_value = solValue(xvals, xcoefs)
        else:
            xvals = []
            aux_value = 0

        solution = list(map(round, self.get_values(self.master_vars)))
        solution_key = toKey(solution + xvals)
        zcoefs = self.get_objective_coefficients(self.master_vars)
        zs_value = solValue(solution, zcoefs)

        if self.debug:
            qnames = list(self.cut_vars.values())
            qvals = self.get_values(qnames)
            qcoefs = self.get_objective_coefficients(qnames)
            incumbent = solValue(qvals, qcoefs)

            assert isZero(
                zs_value + aux_value + incumbent - self.get_objective_value()),\
                "Master objective does not match sum of its components: "\
                "z={} + x={} + q={} =/= {}".format(
                    zs_value, aux_value, incumbent, self.get_objective_value())

        return solution, solution_key, zs_value, aux_value

    def compute_cuts(self):
        """
        Compute Benders cuts, that is: given an (integer) solution, we compute
        the dual coefficients from the LP relaxation of the SPs and,
        potentially, an upper bound from the heuristic. Then, we store this in
        `self.last_cuts` for the appropriate callback to use. We also return
        whether this solution is feasible so the (other) appropriate callback
        can reject it.

        Return: LP relaxation values for the SPs, heuristic values for the SPs
        """
        solution, solution_key, zs_value, aux_value = self.get_obj_vals()

        # Do not repeat computations when we have already explored this
        # solution
        if solution_key in Benders.solutions:
            return None, None

        qnames = list(self.cut_vars.values())
        qvals = self.get_values(qnames)
        qcoefs = self.get_objective_coefficients(qnames)
        incumbent = solValue(qvals, qcoefs)

        Benders.iterations += 1
        gap = None
        relax_value = None
        heur_value = None
        heur_bounds = None
        opti_cut = False

        master_value = zs_value + aux_value
        relax_bounds = self.relaxed_bounding(solution, master_value)

        if relax_bounds is not None:
            relax_value = sum(relax_bounds.values())
            # TODO Equal to relax_bounds is None?
            opti_cut = True
            if self.heuristic is not None:
                heur_bounds = self.heur_bounding(
                    solution, solution_key, master_value)
                heur_value = sum(heur_bounds.values())

                gap = mipGap(Benders.relax_bound, master_value + heur_value)
                Report.int_gap += gap
            else:
                gap = mipGap(Benders.relax_bound, master_value + relax_value)

        self.solutions[solution_key] = (master_value, relax_value, heur_value)

        if self.save_vars:
            self.values[solution_key] = self.get_values(
                self.master_vars + self.aux_vars + self.ext_vars)

        self.progress(Benders.iterations, master_value, incumbent, relax_value,
                      gap, opti_cut, heur_value)

        if (self.max_iterations is not None and
                Benders.iterations >= self.max_iterations):
            self.finish("Maximum iteration reached.")

        if self._check_time(Report.upper_time + Report.sub_time):
            self.finish("Maximum time reached.")

        if self.heuristic is None:
            # Early cut-off for standard Benders: stop when the difference
            # between the incumbent and the current sub-problem's value is zero
            if (relax_value is not None and
                    all(isZero(lb - q) for lb, q in zip(relax_bounds, qvals))):
                self.finish("Found q(z) = q.", early=True)
        # elif gap is not None and Benders.exact_heur and isZero(gap):
        #     # Can only close early if the bounding procedure is exact
        #     self.finish("Closed integer gap.", early=True)

        return relax_bounds, heur_bounds

    def relaxed_bounding(self, solution, master_value):
        with Timer() as t:
            relax_bounds = self._relaxed_bounding(solution, master_value)
        Report.sub_time += t.interval

        return relax_bounds
   
    def _relaxed_bounding(self, solution, master_value):
        """
        Compute and update the relaxed bound given the current solution.
        """
        relax_bounds = {cut_id: 0 for cut_id in self.cut_vars}
        benders_cuts = {
            cut_id: {"duals": [0] * len(self.master_vars), "rhs": 0}
            for cut_id in self.cuts
        }

        # TODO Parrallelize this bit
        for sub_id, cut_id in self.bundles.items():
            sub = self.sub_pbs[sub_id]

            # `solve()` should return whether it found a feasible solution
            # or not.
            opti = sub.solve(solution, self.get_node_data())
            lhs, rhs = sub.results()

            if opti and relax_bounds is not None:
                sub_val = sub.value()
                relax_bounds[cut_id] += sub_val
            else:
                # TODO Verify if we can have partial relaxed bounds in case
                # only some sub-problems are feasible
                relax_bounds = None

            if self.debug:
                if len(lhs) != len(self.master_vars):
                    Log.warning("D: {} - M: {}".format(lhs, self.master_vars))
                    self.exception("Duals shape does not match master vars.")

                try:
                    rhs = float(rhs)
                except:
                    self.exception("RHS is not a number.")

            # Update cut
            cut = benders_cuts[cut_id]
            benders_cuts[cut_id] = {
                "duals": zipWith(cut["duals"], lhs, function=operator.add),
                "rhs": cut["rhs"] + rhs, "opti": opti
            }

        if relax_bounds is not None:
            relax_values = list(relax_bounds.values())
            relax_bound = sum(relax_values)
            lp_relax = master_value + relax_bound

            if self.is_better(lp_relax, Benders.relax_bound):
                Benders.relax_bound = lp_relax

                Log.debug(
                    "Improved relaxed bound at iteration {} to {}.".format(
                        Benders.iterations, Benders.relax_bound))

            if self.debug:
                # Verify incumbents
                self._verify_incumbent(self.cut_vars, relax_bounds)

                # Verify cuts validity
                self._verify_cuts(benders_cuts, solution, relax_bounds)

                # Verify that cuts actually remove the current solution
                # self._verify_sol(benders_cuts, solution)

        self.last_cuts.append(benders_cuts)

        return relax_bounds

    def heur_bounding(self, solution, rep, master_value):
        with Timer() as t:
            heur_bounds = self._heur_bounding(solution, rep, master_value)
        Report.upper_time += t.interval

        return heur_bounds

    def _heur_bounding(self, solution, solution_key, master_value):
        """
        Heuristic bounding procedure, remember it has to match the shape of the
        relaxed bounding.
        """
        heur_bounds = {cut_id: 0 for cut_id in self.cut_vars}
        heur_value = 0

        for sub_id, cut_id in self.bundles.items():
            sup = self.heuristic[sub_id]
            sup.solve(solution)
            value = sup.value()

            if Benders.is_min:
                ub = value
                lb = self.sub_pbs[sub_id].value()
            else:
                lb = value
                ub = self.sub_pbs[sub_id].value()

            if self.debug and self.is_better(ub, lb, default=False):
                Log.warning("[{}] UB < LB: {} < {}, {}".format(
                    cut_id, ub, lb, solution_key))
                self.exception(
                    "Wrong heuristic bound for sub {} at iteration {}.".
                    format(sub_id, Benders.iterations))
            else:
                heur_bounds[cut_id] += value
                heur_value += value

        # Update heuristic bound
        if self.is_better(heur_value + master_value, Benders.heur_bound):
            if self.aux_vars:
                xvals = self.get_values(self.aux_vars)
            else:
                xvals = []

            Benders.heur_bound = heur_value + master_value
            Benders.new_incumbent = solution + xvals + list(heur_bounds.values())

            Log.debug(
                "Improved heuristic bound at iteration {} to {}.".format(
                    Benders.iterations, Benders.heur_bound))

        return heur_bounds


class Cuts(cb.LazyConstraintCallback, BendersCut):
    """
    Generate lazy constraints at given integer solutions.
    """

    def _add_L_shaped_cuts(self, L_shaped, cut_vars, cut_ids, solution):
        z_0 = []
        z_1 = []
        for i, z in enumerate(solution):
            if isZero(z):
                z_0.append(self.master_vars[i])
            else:
                z_1.append(self.master_vars[i])

        for cut_id, L_value in L_shaped.items():
            sub_value = 0
            for sub_id in cut_ids[cut_id]:
                sub_value += self.heuristic[sub_id].value()

            # Difference between the optimal value of the SP and the global
            # lower bound.
            diff = sub_value - L_value

            names = deepcopy(cut_vars[cut_id])
            names.extend(z_0)
            names.extend(z_1)
            coefs = [1] * len(cut_vars[cut_id])
            coefs.extend([diff] * len(z_0))
            coefs.extend([-diff] * len(z_1))
            Report.nb_L_shaped += 1

            self.add(constraint=(names, coefs),
                     rhs=sub_value - diff * len(z_1),
                     sense="G")

    def __call__(self):
        """
        Cut generation process as lazy constraints: we compute the LP
        relaxation and the heuristic value of the current solution, add cuts
        based on the former, and get a new incumbent based on the latter.
        """
        if self.is_unbounded_node():
            Log.warning("Unbounded relaxation found.")
            return

        cuts = self.get_last_cuts()

        if not cuts:            # Duplicate solution
            return

        for benders_cuts in cuts:
            opti_cut = True
            for cut_id, cut in benders_cuts.items():
                names = deepcopy(self.master_vars)
                duals = list(trim(cut["duals"]))

                if cut["opti"]:
                    # Optimality cuts use the incumbent variable(s)
                    names.append(self.cut_vars[cut_id])
                    duals.append(-1.0)

                    Report.nb_opti_cuts += 1
                else:
                    Report.nb_feas_cuts += 1
                    opti_cut = False

                # Have to invert sign as the RHS should be returned as a LHS
                # member
                self.add(
                    constraint=cplex.SparsePair(ind=names, val=duals),
                    rhs=-trim(cut["rhs"]), sense=self.cut_sense)

        # Reset saved cuts
        self.last_cuts = []

        if self.heuristic is not None and opti_cut and self.L_shaped is not None:
            solution, _, _, _ = self.get_obj_vals()
            # assert self.isExactHeur(),\
            #     "L-shaped requires the exact value of the sub-problem."
            if type(self.L_shaped) is dict:
                # Multicut L-shaped
                self._add_L_shaped_cuts(
                    self.L_shaped,
                    {k: [v] for k, v in self.cut_vars.items()},
                    self.cuts,
                    solution)
            else:
                # Single L-shaped
                self._add_L_shaped_cuts(
                    {0: self.L_shaped},
                    {0: list(self.cut_vars.values())},
                    {0: [cid for cut_ids in self.cuts.values()
                         for cid in cut_ids]},
                    solution)


class Cardinal(cb.UserCutCallback, BendersCut):
    """
    This class adds cardinality cuts as soon as possible in the tree.
    """

    def __call__(self):
        """
        Add a cut barring solution with a size (number of active variables)
        greater than their maximum possible contribution. The contribution is
        calculated as the difference between the relaxed bound of the branch and
        the global heuristic bound.
        """
        objective_vars = self.master_vars + self.aux_vars
        feas = self.get_feasibilities(objective_vars)

        # Leaf node
        if sum(feas) == len(objective_vars):
            return

        coefs = self.get_objective_coefficients(objective_vars)
        objective_value = solValue(coefs, self.get_values(objective_vars))
        unset = sorted(
            filter(lambda x: x[0] == 0, zip(feas, coefs)), # Unset vars
            key=lambda x: x[1], reverse=True)

        if type(self.L_shaped) is dict:
            L = sum(self.L_shaped.values())
        else:
            L = self.L_shaped

        # Find the maximum number of master variables that can be active at
        # once in the current branch.
        size = sum(feas)        # Already active set
        ceil = 0
        for z, c in unset:
            ceil += c
            size += 1

            if size >= len(objective_vars):
                return

            if not self.is_better(ceil, Benders.heur_bound - (objective_value + L)):
                break

        Log.debug("Add a cardinality cut with |Z| = {}".format(size))
        Report.nb_H_shaped += 1
        self.add_local((objective_vars, [1] * len(objective_vars)), "L", size)


class Reject(cb.IncumbentCallback, BendersCut):
    """
    This sub-class rejects incumbents that violate the heuristic solutions
    found previously.
    """

    def __call__(self):
        """
        Reject all incumbents found by CPLEX, only accept ones that have been
        generated by `Heuristic::set_solution()`.  Generating new incumbents is
        a two step process for us: use the heuristic callback to set them and
        the incumbent callback to accept them.  Finally, we have to make sure
        that we only accept "user" solutions.

        We also use this callback to *generate cuts*, we cannot post the
        constraints here so we save the results for using later.
        """
        source = self.get_solution_source()
        is_user = self.solution_source.user_solution

        if self.new_incumbent and source == is_user:
            solution = self.get_values(self.master_vars + self.aux_vars)
            solution_key = toKey(solution)
            Log.debug("Accept incumbent for {}".format(solution_key))
            # Reset incumbent
            Benders.new_incumbent = []
        else:
            relax_vals, heur_vals = self.compute_cuts()

            if relax_vals is None:
                self.reject()
            else:
                subgrad = True
                ub = True
                # If the LP relaxation of the SPs exceed their associated
                # incumbent variables, the solution is not feasible.
                for cut_id, cut_var in self.cut_vars.items():
                    value = 0
                    upper = 0
                    for s_id in self.cuts[cut_id]:
                        value += relax_vals[s_id]
                        upper += heur_vals[s_id]

                    subgrad = subgrad and self.is_better(
                        value, self.get_values(cut_var))
                    ub = ub and self.is_better(value, upper)

                if not subgrad:
                    Log.debug(
                        "Reject solution #{} because q^i > q(x)^i".format(
                            Benders.iterations))
                    self.reject()
                elif not ub:
                    Log.debug(
                        "Reject solution #{} because LP > UB".format(
                            Benders.iterations))
                    self.reject()


class Prune(cb.BranchCallback, BendersCut):
    """
    This sub-class can prune branch rooted at the current node.
    """

    def __call__(self):
        """
        Prune nodes whose LP relaxation exceed the best known heuristic
        solution.
        """
        objective = self.get_objective_value()

        if self.is_better(Benders.heur_bound, objective, subopt=True):
            solution_key = toKey(self.get_values(self.master_vars), half=True)
            Log.debug("Fathom branch from {}".format(solution_key))
            # Fathom branch
            self.prune()


class Heuristic(cb.HeuristicCallback, Benders):
    """
    Class used to set incumbent values from heuristic solutions, we hack it to
    update the incumbent only when we find a new one.
    """

    def __call__(self):
        """
        Set the new incumbent value, we save it as a list of values for the
        master problem to which we append the heuristic bounding values.
        """
        # No new incumbent: []
        if Benders.new_incumbent:
            solution = [
                self.master_vars + self.aux_vars + list(self.cut_vars.values()),
                Benders.new_incumbent
            ]

            self.set_solution(solution)
