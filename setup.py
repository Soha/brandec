# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='brandec',
    version='0.2.1',
    description='An (integer) branch-and-benders-cut framework',
    long_description=readme,
    author='Arthur Mahéo',
    author_email='arthur.maheo@anu.edu.au',
    url='https://gitlab.com/Soha/brandec.git',
    license=license,
    python_requires='==3.5.*',    # Because of CPLEX
    packages=find_packages(exclude=('tests', 'docs')),
    install_requires=['numpy'],
    extras_require={
        'dev': ['yapf', 'pytest'],
        'test': ['pytest']})
